#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define A0 0x8000
#define A1 0x4000
#define A2 0x2000
#define A3 0x1000
#define A4 0x0800
#define A5 0x0400
#define A6 0x0200
#define A7 0x0100
#define A8 0x0080
#define A9 0x0040
#define A10 0x0020
#define A11 0x0010
#define A12 0x0008
#define A13 0x0004
#define A14 0x0002
#define A15 0x0001

// lsbit to msbit, to actual values
// msb buffer: a0 a1 a2 a6 a7 a15 a14 a9
int admsb_values[8] = { A0, A1, A2, A6, A7, A15, A14, A9 };
int adlsb_values[8] = { A5, A4, A11, A10, A13, A12, A8, A3 };

int main(int argc, char **argv)
{
    if (argc<2) {
      fprintf(stderr, "Usage: %s byte\n", argv[0]);
      exit(1);
      }

      char *ptr;
      int x = strtol(argv[1], &ptr, 16);

// unscramble bits as lsb or msb addr, or data
   int lsb = 0;
   int msb = 0;
   int lsbmask = 0;
   int msbmask = 0;

    for (int i=0; i<8; ++i) {
      lsb += adlsb_values[i] * (x&1);
      msb += admsb_values[i] * (x&1);
      lsbmask += adlsb_values[i];
      msbmask += admsb_values[i];
      x = x>>1;

    }
      printf("if MSB: %4x  if LSB: %4x\n\n", msb, lsb);
      // print bits with mask
      char lsbstr[34], msbstr[34];
      strcpy(lsbstr, " _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
      strcpy(msbstr, " _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
      
      int bit=0x8000;
      for (int i=1; i<33; i+=2) {
        if(lsbmask&bit) { lsbstr[i] = (lsb&bit ? '1' : '0'); }
        else { lsbstr[i] = '_'; }
        if(msbmask&bit) { msbstr[i] = (msb&bit ? '1' : '0'); }
        else { msbstr[i] = '_'; }

        bit = bit>>1;
      }
      printf("As MSB: %s\n", msbstr);
      printf("As LSB: %s\n", lsbstr);
}




