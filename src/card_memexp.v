
module card_memexp(      input  clk,
                         input  en_cru,  
                         output o_driving_db, 
                         output reg o_data, 
                         input  [15:0] i_addr, 
                         input  [7:0] i_data, 
                         input  i_rd,
                         input  i_we,
                         input  i_cruclk,
                         input  i_cruout,
                         output o_cruin            
);
  wire [7:0] bits;
  cru_register8 cru_reg(.clk(clk), 
                        .en(en_cru), 
                        .i_addr(i_addr[3:1]), 
                        .i_cruclk(i_cruclk), 
                        .i_cruout(i_cruout), 
                        .o_cruin(o_cruin), 
                        .o_bits(bits));
  
  
  reg [7:0] bank_reg[0:15]; // 16 8-bit bank registers
  
  assign memexp = (1 == i_addr[15:13] || 5 == i_addr[15:13] || 6 == i_addr[15:13] || 7 == i_addr[15:13]);
  assign dsr = (4 == i_addr[15:12]) && (0 == i_addr[11:5]);
  assign o_driving_db = i_rd && (dsr || memexp);
  assign bank = i_addr[15:12];
  assign sram_addr = bits[1] ? {bank_reg[bank][7:0],i_addr[11:0]} : {4'd0,i_addr[15:0]};
  assign sram_oe = memexp && (i_rd || i_we);
  assign sram_we = memexp && i_we;
  // store 12 bits using even byte?
    
  assign reg_sel = i_addr[4:1];
  assign lsb = i_addr[0];
  wire   sram_data_out;
  reg    sram_data_in;
  
  always @(posedge clk)
  begin
    
    if (bits[0] && i_we && dsr && lsb)
      bank_reg[reg_sel] = i_data;
    
    if (i_rd)
      o_data <=   memexp ? sram_data_out 
                : dsr ? bank_reg[reg_sel]
                : 0;
      
    if (i_we && memexp)
      sram_data_in <= i_data;
       
  end
    
endmodule