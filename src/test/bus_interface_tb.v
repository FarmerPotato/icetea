  // Test bench for the decoder_state module
  
module test;

  localparam ENCODER_STATE_READCRU  = 0;
  localparam ENCODER_STATE_READ1    = 1;    // reading lsb
  localparam ENCODER_STATE_TRANSIT  = 2;
  localparam ENCODER_STATE_WRITE1   = 3;   // writing lsb
  localparam ENCODER_STATE_WRITECRU = 4;
  localparam ENCODER_STATE_READ2    = 5;    // reading msb
  localparam ENCODER_STATE_WRITE2   = 6;   // writing msb
  localparam ENCODER_STATE_RESET    = 7;
  
  
  reg tests_failed = 0;
  event terminate_sim;

  reg clk, clk12, rst;
  wire phi3;
  wire [2:0] decoder_state;
  reg  [2:0] encoder_state;   // driven by this test
  wire [2:0] snoop;
  
  // inputs to sim
  reg [15:0] addr;
  reg [15:0] data;
  // break out encoder signals
  wire memen, dbin, a15, we, cruclk;
  encoder_state enc(encoder_state, memen, dbin, a15, we, cruclk);
  wire dben1, dben2, dbdir, aben1, aben2, cruen, crubit;
  decoder_state dec(decoder_state, dben1, dben2, dbdir, aben1, aben2, cruen, crubit);
  
  // some outputs
  
  wire [7:0] bus, bus_out;
  wire [15:0] address_out;
  
  icetea_sim sim(.i_decoder_state(decoder_state), 
                 .i_encoder_state(encoder_state), 
                 .o_bus(bus), // 8-bit bus from icetea to fpga
                 .i_addr(addr), 
                 .i_data(data) 
                 );
  
  // need a cpu_sim with bus cycle test vectors
  // need a encoder module making encoder_state
  // until then this has only the test cycle controlled from in here.
  
  wire [7:0] o_data;
  wire [15:0] o_address; // unscrambled cpu address bus
  
  bus_interface busi(.clk(clk)
              , .phi3(phi3)
              , .i_data_bus(bus)
              , .o_data_bus(bus_out) // 8-bit bus in the other direction, to icetea/4a
              , .o_address(address_out)
              , .i_encoder_state(encoder_state)  // encoder lines from icetea, driven here in tb
              , .o_decoder_state(decoder_state)  // driven by real module
              );
  
  always #5    clk = !clk;
  
  //  always #41.666 clk12 = !clk12;
  always #5 clk12 = !clk12;// temporary speedup
  clockgen clockg(.clk12(clk12), .phi3(phi3));
  
//  always #10   decoder_state = decoder_state_next;
    
  initial begin
     clk = 0;
     clk12 = 0;
     rst = 0;
     addr = 16'h83e0;
     data = 16'h1234;
     encoder_state     = ENCODER_STATE_RESET;
     
     #10 rst = 1;
     #10 rst = 0;
     #10 encoder_state = ENCODER_STATE_READCRU;
     #70 encoder_state = ENCODER_STATE_READ1;
     #100 encoder_state = ENCODER_STATE_READ2;
     #100 encoder_state = ENCODER_STATE_WRITE1;
     #100 encoder_state = ENCODER_STATE_WRITE2;
     #100 encoder_state = ENCODER_STATE_WRITECRU;
     #200 $stop;
  end

  initial
          $monitor("At time %t, rst=%d, state=%d, aben1=%d, aben2=%d, addr=%x data=%x a15=%d bus=%x bus_out=%x addr_out=%x", 
            $time, rst, encoder_state, aben1, aben2, addr, data, a15, bus, bus_out, address_out);
            
              
endmodule // test