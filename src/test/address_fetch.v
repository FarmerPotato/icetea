module address_fetch(
  input clk,
  input request,
  output ready, 
  input [7:0] bus_in,
  output aben1, 
  output aben2,
  output reg [15:0] addr_out,
  output [2:0] state_out);

  // local states
  localparam [2:0] START  = 0,
                   READ1  = 2,
                   READ2  = 5,
                   DECODE = 6,
                   DONE   = 7;

	reg [15:0] addr_reg;
  
  // combinatorial logic
  assign aben1 = !(state == 0 || state == 1 || state == 2);
  assign aben2 = !(state == 3 || state == 4 || state == 5);
  assign ready =  (state == DONE);
  assign state_out = state;
  
  // state register
  reg [2:0]  state, state_next;
  
  always @(posedge clk, posedge request)
  begin
    if (request) 
      state <= START;
    else
      state <= state_next;
  end
  
  
  // next-state logic and register transfer
  
	always @(posedge clk)
	begin
	  if (state != DONE)
  	  state_next <= state + 1;
	  else
	    state_next <= DONE;
	   
	  if (state == READ1)
      addr_reg[15:8] = bus_in;  
    else if (state == READ2)
      addr_reg[7:0]  = bus_in;
    else if (state == DECODE)
			// unscramble bits, because it was easier than routing lines on the pcb
			//       BD7   6   5   4   3   2    1  0
			// ABEN1  A0  A1  A6  A2  A9  A14 A15 A7
			// ABEN2  A5  A4  A10 A11 A3  A8  A12 A13
			//
				addr_out[15:0] = {
					addr_reg[15],
					addr_reg[14],
					addr_reg[12],
					addr_reg[3],
					addr_reg[6],
					addr_reg[7],
					addr_reg[13],
					addr_reg[8],
					addr_reg[2],
					addr_reg[11],
					addr_reg[5],
					addr_reg[4],
					addr_reg[1],
					addr_reg[0],
					addr_reg[10],
					addr_reg[9]
        };
  end
endmodule