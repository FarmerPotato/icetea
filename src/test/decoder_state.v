/*
  The 3-bit decoder state is sent from FPGA to IceTea to drive the bus enables (or CRU).
  This module is used in simulation.
*/

module decoder_state(
  input [2:0] i_decoder_state,
  output dben1, dben2, dbdir, aben1, aben2, cruen, crubit
  );
  
  localparam DECODER_STATE_WRITE1   = 0; // (cpu is writing, accept either lsb or msb) 
  localparam DECODER_STATE_WRITE2   = 1; // future use for 16 bit data bus
  localparam DECODER_STATE_READ1    = 2; // (cpu is reading, provide either lsb or msb)
  localparam DECODER_STATE_READ2    = 3; // future use for 16 bit data bus
  localparam DECODER_STATE_ADDR1    = 4; // read addr1 scrambled on v02, MSB on v03
  localparam DECODER_STATE_ADDR2    = 5; // read addr2 scrambled on v02, LSB on v03
  localparam DECODER_STATE_CRU_LOW  = 6;
  localparam DECODER_STATE_CRU_HIGH = 7;

	assign dben1 = !(i_decoder_state == DECODER_STATE_WRITE1 || i_decoder_state == DECODER_STATE_READ1);
	assign dben2 = !(i_decoder_state == DECODER_STATE_WRITE2 || i_decoder_state == DECODER_STATE_READ2);
	assign aben1 = !(i_decoder_state == DECODER_STATE_ADDR1);
	assign aben2 = !(i_decoder_state == DECODER_STATE_ADDR2);
	// dbdir=1 for cpu writing to IceTea, the safe and usual assumption
	assign dbdir = !(i_decoder_state == DECODER_STATE_READ1 || i_decoder_state == DECODER_STATE_READ2);
	assign cruen = !(i_decoder_state == DECODER_STATE_CRU_HIGH || i_decoder_state == DECODER_STATE_CRU_LOW);
  assign crubit = (i_decoder_state == DECODER_STATE_CRU_HIGH) ? 1 : ((i_decoder_state == DECODER_STATE_CRU_LOW) ? 0 : 3'bz);
endmodule
