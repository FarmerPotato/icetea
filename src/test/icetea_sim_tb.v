  // Test bench for the decoder_state module
  
module test;

  reg [2:0] state;
  wire dben1, dben2, dbdir, aben1, aben2, cruen, crubit;
  


  // decoder_state controls the buffers on IceTea
  localparam DECODER_STATE_WRITE1   = 0; // (cpu is writing, accept either lsb or msb) 
  localparam DECODER_STATE_WRITE2   = 1; // future use for 16 bit data bus
  localparam DECODER_STATE_READ1    = 2; // (cpu is reading, provide either lsb or msb)
  localparam DECODER_STATE_READ2    = 3; // future use for 16 bit data bus
  localparam DECODER_STATE_ADDR1    = 4; // read addr1 scrambled on v02, MSB on v03
  localparam DECODER_STATE_ADDR2    = 5; // read addr2 scrambled on v02, LSB on v03
  localparam DECODER_STATE_CRU_LOW  = 6;
  localparam DECODER_STATE_CRU_HIGH = 7;
  
  
  reg tests_failed = 0;
  event terminate_sim;

  reg [15:0] addr;
  reg [15:0] data;
  reg        a15;
  input [7:0] bus;

  icetea_sim sim(state, bus, addr, data, a15, phi3);
  
  initial begin
     
     addr = 16'h83e0;
     data = 16'h1234;
     a15 = 1;
     state = 4;
     
     # 50 state = 5;
     # 50 state = 0; a15 = 1;
     # 50 state = 0; a15 = 0;
     
     # 10 $stop;
     
  end
  
  initial
          $monitor("At time %t, state=%d, addr=%x data=%x a15=%d bus=%x", 
            $time, state, addr, data, a15, bus);
            
              
endmodule // test