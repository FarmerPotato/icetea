/*
  The 3-bit decoder state is sent from FPGA to IceTea to drive the bus enables (or CRU).
  This module is used in simulation.
*/

module encoder_state(
  input [2:0] i_encoder_state,
  output memen, dbin, a15, we, cruclk
  );
  
  localparam ENCODER_STATE_READCRU  = 0;
  localparam ENCODER_STATE_READ1    = 1;    // reading lsb
  localparam ENCODER_STATE_TRANSIT  = 2;
  localparam ENCODER_STATE_WRITE1   = 3;   // writing lsb
  localparam ENCODER_STATE_WRITECRU = 4;
  localparam ENCODER_STATE_READ2    = 5;    // reading msb
  localparam ENCODER_STATE_WRITE2   = 6;   // writing msb
  localparam ENCODER_STATE_RESET    = 7;

	assign memen = 
	  (i_encoder_state == ENCODER_STATE_READ1)    ? 0 :
	  (i_encoder_state == ENCODER_STATE_READ2)    ? 0 :
	  (i_encoder_state == ENCODER_STATE_WRITE1)   ? 0 :
	  (i_encoder_state == ENCODER_STATE_WRITE2)   ? 0 :
	  (i_encoder_state == ENCODER_STATE_TRANSIT)  ? 0 :
	  (i_encoder_state == ENCODER_STATE_RESET)    ? 1 :
	  (i_encoder_state == ENCODER_STATE_READCRU)  ? 1 :
	  (i_encoder_state == ENCODER_STATE_WRITECRU) ? 1 : 1;
	  
 	assign dbin = 
	  (i_encoder_state == ENCODER_STATE_READ1)   ? 0 :
	  (i_encoder_state == ENCODER_STATE_READ2)   ? 0 : 1;

	assign a15 = 
	  (i_encoder_state == ENCODER_STATE_READ1)    ? 0 :
	  (i_encoder_state == ENCODER_STATE_READ2)    ? 1 :
	  (i_encoder_state == ENCODER_STATE_WRITE1)   ? 0 :
	  (i_encoder_state == ENCODER_STATE_WRITE2)   ? 1 :
	  (i_encoder_state == ENCODER_STATE_TRANSIT)  ? 1'bx :
	  (i_encoder_state == ENCODER_STATE_RESET)    ? 1'bx :
	  (i_encoder_state == ENCODER_STATE_READCRU)  ? 1'bx :
	  (i_encoder_state == ENCODER_STATE_WRITECRU) ? 1'bx : 1'bx; // don't know
	  
 	assign we = 
	  (i_encoder_state == ENCODER_STATE_WRITE1)   ? 0 :
	  (i_encoder_state == ENCODER_STATE_WRITE2)   ? 0 : 1;


  assign cruclk = 
    (i_encoder_state == ENCODER_STATE_WRITECRU) ? 0 : 1;
 

	
	endmodule
