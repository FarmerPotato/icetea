  // Test bench for the decoder_state module
  
module test;

  // decoder_state controls the buffers on IceTea
  localparam DECODER_STATE_WRITE1   = 0; // (cpu is writing, accept either lsb or msb) 
  localparam DECODER_STATE_WRITE2   = 1; // future use for 16 bit data bus
  localparam DECODER_STATE_READ1    = 2; // (cpu is reading, provide either lsb or msb)
  localparam DECODER_STATE_READ2    = 3; // future use for 16 bit data bus
  localparam DECODER_STATE_ADDR1    = 4; // read addr1 scrambled on v02, MSB on v03
  localparam DECODER_STATE_ADDR2    = 5; // read addr2 scrambled on v02, LSB on v03
  localparam DECODER_STATE_CRU_LOW  = 6;
  localparam DECODER_STATE_CRU_HIGH = 7;
  
  
  reg tests_failed = 0;
  event terminate_sim;

  reg clk, rst;
  reg [2:0] state;
  reg [2:0] state_next;
  wire [2:0] snoop;
  
  reg [15:0] addr;
  wire [15:0] addr_out;
  reg [15:0] data;
  reg        a15;
  wire [7:0] bus;

  icetea_sim sim(state, addr, data, a15, bus);
  address_fetch fet(clk, rst, ready, bus, aben1, aben2, addr_out, snoop);

  always #5    clk = !clk;
  always #10   state = state_next;
    
  initial begin
     clk = 0;
     rst = 0;
     addr = 16'h83e0;
     data = 16'h1234;
     a15 = 1;

     
     #10 rst = 1;
     #10 rst = 0;
     #200 $stop;
  end

  always @*
  begin
    if(!aben1) 
      state_next = DECODER_STATE_ADDR1;
    else if(!aben2) 
      state_next = DECODER_STATE_ADDR2; 
    else 
      state_next = DECODER_STATE_WRITE1; 
  end
  
  initial
          $monitor("At time %t, rst=%d, state=%d, aben1=%d, aben2=%d, addr=%x data=%x a15=%d bus=%x ready=%d addr_out=%x, state_next=%d, snoop=%d", 
            $time, rst, state, aben1, aben2, addr, data, a15, bus, ready, addr_out, state_next, snoop);
            
              
endmodule // test