  // Test bench for the decoder_state module
  
module test;

  reg [2:0] state;
  wire dben1, dben2, dbdir, aben1, aben2, cruen, crubit;
  


  // decoder_state controls the buffers on IceTea
  localparam DECODER_STATE_WRITE1   = 0; // (cpu is writing, accept either lsb or msb) 
  localparam DECODER_STATE_WRITE2   = 1; // future use for 16 bit data bus
  localparam DECODER_STATE_READ1    = 2; // (cpu is reading, provide either lsb or msb)
  localparam DECODER_STATE_READ2    = 3; // future use for 16 bit data bus
  localparam DECODER_STATE_ADDR1    = 4; // read addr1 scrambled on v02, MSB on v03
  localparam DECODER_STATE_ADDR2    = 5; // read addr2 scrambled on v02, LSB on v03
  localparam DECODER_STATE_CRU_LOW  = 6;
  localparam DECODER_STATE_CRU_HIGH = 7;
  
  
  reg tests_failed = 0;
  event terminate_sim;

  decoder_state dec(state, dben1, dben2, dbdir, aben1, aben2, cruen, crubit);
  
  initial begin
     
     state = 0;
     # 10 state = 1;
     # 10 state = 2;
     # 10 state = 3;
     # 10 state = 4;
     # 10 state = 5;
     # 10 state = 6;
     # 10 state = 7;
     # 10 state = 0;
     # 10 $stop;
     
  end
  
  initial
          $monitor("At time %t, state=%d dben1=%d dben2=%d dbdir=%d aben1=%d aben2=%d cruen=%d crubit=%d",
            $time, state, dben1, dben2, dbdir, aben1, aben2, cruen, crubit);

              
endmodule // test