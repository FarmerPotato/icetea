/*
  The 3-bit decoder state is sent from FPGA to IceTea to drive the bus enables (or CRU).
  This module is used in simulation.
*/

module icetea_sim(
  input [2:0]  i_decoder_state,
  input [2:0]  i_encoder_state,
  output reg [7:0] o_bus,
  input [15:0] i_addr,
  input [15:0] i_data,
  input wire   i_a15
  );
  
  wire dben1, dben2, dbdir, aben1, aben2, cruen, crubit;
  
  localparam DECODER_STATE_WRITE1   = 0; // (cpu is writing, accept either lsb or msb) 
  localparam DECODER_STATE_WRITE2   = 1; // future use for 16 bit data bus
  localparam DECODER_STATE_READ1    = 2; // (cpu is reading, provide either lsb or msb)
  localparam DECODER_STATE_READ2    = 3; // future use for 16 bit data bus
  localparam DECODER_STATE_ADDR1    = 4; // read addr1 scrambled on v02, MSB on v03
  localparam DECODER_STATE_ADDR2    = 5; // read addr2 scrambled on v02, LSB on v03
  localparam DECODER_STATE_CRU_LOW  = 6;
  localparam DECODER_STATE_CRU_HIGH = 7;

  // test utility unpack the states into their bits
  decoder_state dec(i_decoder_state, dben1, dben2, dbdir, aben1, aben2, cruen, crubit);
  encoder_state enc(.i_encoder_state(i_encoder_state), .a15(i_a15));
	
	always #10 begin
					 if (0 == dben1)  o_bus = (i_a15 ? i_data[7:0] : i_data[15:8]);
			else if (0 == aben1)  o_bus = i_addr[15:8];
			else if (0 == aben2)  o_bus = i_addr[8:0];
			else o_bus = 8'bz;
	end
	  
	endmodule
