  // Test bench for the cru_register8 module
  
module test;
  
  reg tests_failed = 0;
  event terminate_sim;
  
  reg  clk;
  
  reg [15:0] addr;

  reg en_cru;
  wire cruin;
  reg  cruout;
  reg  cruclk; 
  
  wire  [7:0] bits;
  
  cru_register8 ls259(.clk(clk), .en(en_cru), .i_addr(addr[3:1]), .i_cruclk(cruclk), .i_cruout(cruout), .o_cruin(cruin), .o_bits(bits));
  
  always #5    clk = !clk;

  initial begin
     clk = 0;
     en_cru = 1;
     addr = 16'h1e00;
     cruclk = 1;
     
     # 10 en_cru = 1; cruclk = 0; cruout = 1;
     # 10 addr = addr + 2; cruclk = 1; cruout = 0;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 1;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 0;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 1;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 0;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 1;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 0;
     # 10 cruclk = 0;
     # 10 addr = addr + 2; cruclk = 1; cruout = 0;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 addr = addr + 2;
     # 10 en_cru = 0;
     
     # 10 $stop;
     
  end
  
  initial
          $monitor("At time %t, clk=%d en_cru=%d cruout=%d cruclk=%d addr=%x bits=%8b cruin=%d",
            $time, clk, en_cru, cruout, cruclk, addr, bits, cruin);

              
endmodule // test