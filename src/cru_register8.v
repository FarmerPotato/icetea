module cru_register8(input clk,
                         input en,                           
                         input [2:0] i_addr, 
                         input i_cruclk,
                         input i_cruout,
                         output o_cruin,
                         output [7:0] o_bits
                      );
  // An 8-bit LS259 addressible register
  
  reg [7:0] crubits;

  assign o_cruin = crubits[i_addr];
  assign o_bits = crubits;
  
  always @(posedge clk)
  begin
    if (en && !i_cruclk)
      crubits[i_addr] <= i_cruout;
  end

endmodule


