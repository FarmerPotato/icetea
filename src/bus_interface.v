
module bus_interface(
    // Host control lines and memory bus
    input  wire clk,
    input  wire reset,
    input  wire phi3,
	
	// outputs to memory users:
    output wire [15:0] o_address, // unscrambled cpu address bus
    output wire o_address_valid,
	input  wire [7:0] i_data_bus, // data bus to this module to go toward cpu
    output wire [7:0] o_data_bus, // data bus from the cpu
	output wire o_rd,               // positive logic, internal read pulse
	output wire o_we,               // positive logic, internal write pulse
	output wire o_cruclk,           // read=1, write=0
	input  wire i_cruin,            // cru bit to cpu
    input  wire i_cru_en,           // cru address read is one of ours
	output wire o_cruout,           // cru bit from cpu
	
    output wire [2:0] o_decoder_state,   // drives external buffers
    input  wire [2:0] i_encoder_state,    // 3 bits of state encoded externally

    // test bench monitoring only
    output wire [2:0] o_state, 
    output wire o_memory_cycle_begin,
    output wire [5:0] o_count,
    // SRAM pins - move to sram_interface!
    output wire o_sram_data_out_en,       // when to switch between in and out on data pins
    output wire [17:0] o_sram_address,    // address pins of the 512KB SRAM, 16 bit word address
    input  wire [15:0] i_sram_data,
    output wire [15:0] o_sram_data,
    output wire RAMOE,                     // output enable - low to enable
    output wire RAMWE,                     // write enable - low to enable
    output wire RAMCS                     // chip select - low to enable
    );

  wire encoder_state = i_encoder_state;    
  wire decoder_state = o_decoder_state;    
// Service a memory read or write cycle from the CPU.
// TODO:
// keep databus Z unless address is to one of our pages.
// decode memory mapped devices.
// add CRU write to enable DSR roms (in cru_interface.v)
// add GROM and VDP memory access.

  reg  [2:0]  state_reg, state_next;
  reg  [2:0]  decoder_state_reg, decoder_state_next;
  reg  [15:0] addr_reg, addr_next; // the decoded, and pre-decoded address bits
  reg         address_valid, address_valid_next;
  reg  [15:0] o_data_reg, i_data_reg;
  reg  [2:0]  waits_reg, waits_next;
  //wire shift_reset;
  //wire addr_in_done;
  
  // logic to SRAM
  reg output_enable, output_enable_next; // neg logic to sram
  reg write_enable, write_enable_next;  // neg logic to sram
  reg chip_select, chip_select_next; // neg logic to sram, rdbena (for now)
  reg [17:0] external_addr;  // address of a 16-bit word in sram
  reg [15:0] data_read_reg;  // 16 bit word
  reg [15:0] data_write_reg; // 16 bit word

	// Memory Mapper interface
	reg [3:0] bank_sel;
	wire bank_mapped;
	wire bank_readonly;
  wire [6:0] bank_address;


  // SRAM interface
			
  // pins direction to SRAM, right?    
	assign o_sram_data_out_en = (state_reg == STATE_WRITE_MEM); // positive logic. turn on output pins when writing data
	assign o_sram_address = external_addr;
	assign o_sram_data = o_data_reg;

	assign RAMOE = output_enable; 
	assign RAMWE = write_enable;
	assign RAMCS = chip_select;

	
	// Memory mapped devices
  // 8000 internal RAM
  // 8400 sound
  // 8800 VDPRD
  //    2 VDPSTA
  // 8C00 VDPWD
  //    2 VDPWA
  // 9000 SPCHRD
  // 9400 SPCHWD
  // 9800 GRMRD
  //    2 GRMRA
  // 9C00 GRMWD
  //    2 GRMWA
  
  // encoder_state (guess) from IceTea
  // 000 idle
  // 001
  // 010
  // 011
  // 100
  // 101
  // 110
  // 111 
  // read/write from CPU point of view
  localparam ENCODER_STATE_READCRU  = 0;
  localparam ENCODER_STATE_READ1    = 1;    // reading lsb
  localparam ENCODER_STATE_TRANSIT  = 2;
  localparam ENCODER_STATE_WRITE1   = 3;   // writing lsb
  localparam ENCODER_STATE_WRITECRU = 4;
  localparam ENCODER_STATE_READ2    = 5;    // reading msb
  localparam ENCODER_STATE_WRITE2   = 6;   // writing msb
  localparam ENCODER_STATE_RESET    = 7;
  
  // decoder_state controls the buffers on IceTea
  localparam DECODER_STATE_ADDR1    = 0;
  localparam DECODER_STATE_ADDR2    = 1;
  localparam DECODER_STATE_READ1    = 2; // (cpu is reading, provide either lsb or msb)
  localparam DECODER_STATE_READ2    = 3; // future use for 16 bit data bus
  localparam DECODER_STATE_WRITE1   = 4; // (cpu is writing, accept either lsb or msb) 
  localparam DECODER_STATE_WRITE2   = 5; // future use for 16 bit data bus
  localparam DECODER_STATE_CRU_LOW  = 6;
  localparam DECODER_STATE_CRU_HIGH = 7;
  
  // Internal states mostly following encoder_state
  
  localparam STATE_IDLE       = 3'b000;
  localparam STATE_OBTAIN_ADDRESS_1 = 3'b001;
  localparam STATE_OBTAIN_ADDRESS_2 = 3'b010;
  localparam STATE_DECODE_ADDRESS   = 3'b011;
  localparam STATE_READ_MEM         = 3'b100;
  localparam STATE_WRITE_MEM        = 3'b101;
  localparam STATE_READ_CRU         = 3'b110;
  localparam STATE_WRITE_CRU        = 3'b111;

  localparam WAIT_CYCLES            = 3; // wait 30 ns for gates to settle (generous)
    
  // A15 changing is a kind of substate
  assign o_data_bus = 
	  (encoder_state == ENCODER_STATE_READ1) ? o_data_reg[15:8] :
    (encoder_state == ENCODER_STATE_READ2) ? o_data_reg[7:0]  :
    8'hzz; 


  // intended for memory mapped devices? or waiting for memory read/write to be granted?
//  localparam STATE_READ_IO =  8; 
//  localparam STATE_WRITE_IO = 9;

  // States:
  //
  // STATE_IDLE
  //   at powerup, load roms into SRAM. 
  // STATE_IDLE
  //   address is valid (at negedge(phi3) and !memen)
  //   raise o_memory_cycle_begin.
  // STATE_OBTAIN_ADDRESS_1
  //   read address from 8-bit port
  // STATE_DECODE_ADDRESS
  //   read bank register address
  //   write address to either pins in or pins out
  //
  // STATE_READ_MEM
  //   obtain data word from SRAM

  // STATE_READ_IO

  //
  // STATE_WRITE_MEM
  //   obtain data word from host databus
  // STATE_WRITE_IO



/////////////////////////////////////////////////////////////
// Sequential logic
/////////////////////////////////////////////////////////////

  always @(posedge clk, posedge reset) begin
   if (reset) begin
     state_reg     <= STATE_IDLE;
     decoder_state_reg <= DECODER_STATE_ADDR1;
     addr_reg      <= 16'h0000;
     chip_select   <= 1; // active low
     write_enable  <= 1;
     output_enable <= 1;
   end else begin
     state_reg     <= state_next;
     decoder_state_reg <= decoder_state_next;
     waits_reg     <= waits_next;    
     addr_reg      <= addr_next;
	 address_valid <= address_valid_next;

     chip_select   <= chip_select_next;
     write_enable  <= write_enable_next;
     output_enable <= output_enable_next;
   end
  end
  
  assign o_state       = state_reg; // debug
  assign o_address_bus = addr_reg;
  
  // maybe waiting for negedge phi3 should be a state_reg.
  // Tie negedge phi3 into the state_reg transition, while only assigning state_reg in the always @(clk)
  // address is settled 100ns after negdge phi1. living dangerously would be to sample address at posedge phi3. (83 ns)
//  always @(negedge phi3) begin
//    if (state_reg == STATE_IDLE)
//      o_memory_cycle_begin = 1;
//    else
//      o_memory_cycle_begin = 0;
//  end
  
				
	// LVC245A data bus buffer
	// DIR 0: b->a, 1: a->b
	// OE* 0: enabled 1: disabled
	//
	// DBDIR (on ATF22LV10C) = !(DECODER_STATE_READ1 || DECODER_STATE_READ2)
	// OE = (read or) write state
	// DBDIR must only be 0 if one of our addresses is being read, in a memory read cycle.
	//
	
	// dbin=1 indicates the 9900 is expecting input. But we mustnt respond at every address.
  

	//assign o_rdbena = !( is_memexp && (state_reg == STATE_READ_MEM || state_reg == STATE_WRITE_MEM)); // LVC245A output enable*

/////////////////////////////////////////////////////////////
// state_next logic and case branching
/////////////////////////////////////////////////////////////
  reg last_phi3;
    
  always @*
  begin
    state_next         = state_reg;
    decoder_state_next = decoder_state_reg;
    last_phi3          = phi3;
    addr_next          = addr_reg;

    case (state_reg)
    // Idle state_reg. an address read can begin on any negedge(phi3)
    STATE_IDLE : 
    begin
      address_valid_next = 0;
	  decoder_state_next = DECODER_STATE_ADDR1; // in idle, be reading address
      //bank_sel <= 4'b0000;
		  chip_select_next   <= 1; // neg logic
		  write_enable_next  <= 1;
		  output_enable_next <= 1;

		  // exit idle state based on incoming encoder_state and low phi3 (or is that too slow?)
		  // do we need to watch for the CHANGE in encoder_state?
      if (!phi3) begin
        if (encoder_state == ENCODER_STATE_RESET)
				  begin
   		      state_next <= STATE_IDLE;
   		      decoder_state_next <= DECODER_STATE_ADDR1;
   		    end
   		  else begin // the idle state is a READCRU even if no one is listening
   		    state_next <= STATE_OBTAIN_ADDRESS_1;
          decoder_state_next <= DECODER_STATE_ADDR1;
          waits_next = WAIT_CYCLES;
        end
      end
    end      

     
    STATE_OBTAIN_ADDRESS_1 :
      begin
				decoder_state_next =  DECODER_STATE_ADDR1;
				if (waits_reg == 0)
				begin
					// TODO: set our 8-bit PMOD to input
					addr_next[15:8] = i_data_bus[7:0];
					// one cycle ahead setting of bank register. in v02, 3 bits will have be enough.
					bank_sel = {i_data_bus[7], i_data_bus[6], i_data_bus[4], 1'b0};
					state_next = STATE_OBTAIN_ADDRESS_2;
					decoder_state_next = DECODER_STATE_ADDR2;
					waits_next = WAIT_CYCLES;
				end
				else
					waits_next = waits_reg-1;  
      end
      
      
    STATE_OBTAIN_ADDRESS_2 :
      begin
				decoder_state_next =  DECODER_STATE_ADDR2;
				if (waits_reg == 0)
				begin
					addr_next[7:0] = i_data_bus[7:0];
					state_next = STATE_DECODE_ADDRESS;
					
					
				end
				else
					waits_next = waits_reg-1;  
      end

    STATE_DECODE_ADDRESS : // 011
      begin

			// v02: unscramble bits, because it was easier than routing lines on the pcb
			//       BD7   6   5   4   3   2    1  0
			// ABEN1  A0  A1  A6  A2  A9  A14 A15 A7
			// ABEN2  A5  A4  A10 A11 A3  A8  A12 A13
			//
			// v03: not needed
				addr_reg[15:0] = {
					addr_next[15],
					addr_next[14],
					addr_next[12],
					addr_next[3],
					addr_next[6],
					addr_next[7],
					addr_next[13],
					addr_next[8],
					addr_next[2],
					addr_next[11],
					addr_next[5],
					addr_next[4],
					addr_next[1],
					addr_next[0],
					addr_next[10],
					addr_next[9]
        };
		address_valid_next = 1;
		
        bank_sel = {addr_next[15], addr_next[14], addr_next[12], addr_next[3]};
        // SRAM pins:
        // bank register should be ready to use
				//external_addr <= {bank_address[6:0], 
				external_addr[17:0] = {3'b000, 					
				  addr_next[15],
					addr_next[14],
					addr_next[12],
					addr_next[3],
					addr_next[6],
					addr_next[7],
					addr_next[13],
					addr_next[8],
					addr_next[2],
					addr_next[11],
					addr_next[5],
					addr_next[4],
					addr_next[1],
					addr_next[0],
					addr_next[10],
					addr_next[9]
        };
        // enable RAM in 32K expansion space only
				case({addr_next[15:14],addr_next[12]})
					3'b001: chip_select_next = 0;
					3'b101: chip_select_next = 0;
					3'b110: chip_select_next = 0;
					3'b111: chip_select_next = 0;
					default: chip_select_next = 1;
				endcase

      // after decode, go to one of 4 specific actions

        case (encoder_state)
          ENCODER_STATE_READCRU : begin
            state_next = STATE_READ_CRU;
          end
          ENCODER_STATE_WRITECRU : begin
            decoder_state_next = DECODER_STATE_ADDR2; // just in case we want A15
            state_next = STATE_WRITE_CRU;
          end
          ENCODER_STATE_READ1 : begin
            decoder_state_next = DECODER_STATE_READ1;
            state_next = STATE_READ_MEM;          
          end
          ENCODER_STATE_READ2 : begin // paranoid
            decoder_state_next = DECODER_STATE_READ2;
            state_next = STATE_READ_MEM;          
          end
          ENCODER_STATE_WRITE1 : begin
            decoder_state_next = DECODER_STATE_WRITE1;
            state_next = STATE_WRITE_MEM;
          end
          ENCODER_STATE_WRITE2 : begin // paranoid
            decoder_state_next = DECODER_STATE_WRITE2;
            state_next = STATE_WRITE_MEM;
          end
          ENCODER_STATE_RESET : begin // paranoid
            decoder_state_next = DECODER_STATE_ADDR1;
            state_next = STATE_IDLE;
          end
          ENCODER_STATE_TRANSIT : begin // I think this occurs before a WE pulse
            decoder_state_next = decoder_state;
            state_next = state_reg;
          end
        endcase          
				
//				write_enable  <= !dbin && !bank_readonly; // enable low
				// write_enable_next  <= dbin; // enable low. dpn't do this until everything out is stable
				// if bank_readonly, the write will still appear on the data pins out, but SRAM will not be enabled to accept it.
					
//				state_next  <= dbin ? STATE_READ_MEM : STATE_WRITE_MEM;

      end    
    STATE_READ_MEM : begin
        // SRAM chip select now 0 only if the address was decoded.
        // this might still be a memory mapped i/o address.
				// fetch word from SRAM
				output_enable_next <= 0; // enable low
				o_data_reg <=  i_sram_data; // 16'h2000;

				decoder_state_next = DECODER_STATE_READ1; // READ2 is reserved for 16 bit machine
			  // in STATE_READ_MEM, let combinatorial logic drive o_data_bus.

			  if (encoder_state != ENCODER_STATE_READ1 && encoder_state != ENCODER_STATE_READ2)
			  begin // break out of read state
			    state_next <= STATE_IDLE;
        end

      end

    
    STATE_WRITE_MEM : begin
        // SRAM chip select now 0 only if the address was decoded.
        // this might still be a memory mapped i/o address.
				
				// gather word from bus
        if (encoder_state == ENCODER_STATE_WRITE1)
        begin
          decoder_state_next <= DECODER_STATE_WRITE1;
          o_data_reg[15:8] <= i_data_bus;
          waits_next = 2; // time to wait after WRITE2
        end
        else if (encoder_state == ENCODER_STATE_WRITE2)
        begin
          decoder_state_next <= DECODER_STATE_WRITE1; // WRITE2 is reserved for 16 bit machine
			   
          o_data_reg[7:0] <= i_data_bus;
          if (waits_reg == 0)
          begin
            // we must fall and latch after data is stable
  				  write_enable_next <= 0; // enable low
  				  // we'll break out of this state when encoder tells us to.
  				end
        end
        else if (encoder_state != ENCODER_STATE_TRANSIT)
        begin
          state_next <= STATE_IDLE; // paranoid
          // TRANSIT occurs between WE pulses
				end
				// else loop here while WRITE1, TRANSIT, or WRITE2.

      end
			  
    STATE_WRITE_CRU : begin
				//stay in READ or WRITE cru state. combinatorial logic does the job.
				// BUG: address could change after N cycles. Multi cycle cru gives no indication.
				// how is phi3 related?
				//assign o_cruout = addr_reg[0];
				//assign o_cruclk = decoder_state != STATE_WRITE_CRU;
				decoder_state_next = DECODER_STATE_ADDR2; // stop driving cruin to cpu. 
				if (encoder_state == ENCODER_STATE_READCRU)
					state_next = STATE_READ_CRU;
				else if (encoder_state == ENCODER_STATE_WRITECRU)
					// follow a15?
                    state_next = STATE_WRITE_CRU;
	            else
					state_next = STATE_IDLE;
				
      end
    
    STATE_READ_CRU : begin
				// caller takes responsibility for setting i_cruin, i_cru_en
				if (i_cru_en)
					if (i_cruin)
						decoder_state_next <= DECODER_STATE_CRU_HIGH;
					else  
						decoder_state_next <= DECODER_STATE_CRU_LOW;
				else 
				    decoder_state_next <= DECODER_STATE_ADDR2; // stop driving cruin to cpu
				
			
				if (encoder_state == ENCODER_STATE_WRITECRU)
					state_next <= STATE_WRITE_CRU;
                else if (encoder_state != ENCODER_STATE_READCRU)
					state_next <= STATE_IDLE;

				
    end		
  endcase
  end

/////////////////////////////////////////////////////////////
// Combinatorial logic
/////////////////////////////////////////////////////////////
  
// debugging
  assign o_memory_cycle_begin = (state_reg == STATE_OBTAIN_ADDRESS_1 ? 1'b1 : 1'b0);

// memory
  assign o_address_valid = address_valid;
// cru 
  assign o_cruout = addr_reg[0];
  assign o_cruclk = decoder_state != STATE_WRITE_CRU;
				
  
endmodule
  