VERILOG_MODULES = bus_interface.v cru_register8.v card_memexp.v card_master.v sn76489.v
TEST_MODULES    = test/icetea_sim.v test/decoder_state.v test/encoder_state.v test/clockgen.v

target: cru_tb;

DECODER_TB_FILES = test/decoder_state_tb.v $(TEST_MODULES)

decoder_tb: $(DECODER_TB_FILES)
	iverilog -o $@ $(DECODER_TB_FILES)
	vvp decoder_tb

SIM_TB_FILES = test/icetea_sim.v test/icetea_sim_tb.v $(TEST_MODULES)

sim_tb: $(SIM_TB_FILES)
	iverilog -o $@ $(SIM_TB_FILES)
	vvp $@

ADDR_TB_FILES = test/address_fetch_tb.v test/address_fetch.v $(TEST_MODULES)

address_fetch_tb: $(ADDR_TB_FILES)
	iverilog -o $@ $(ADDR_TB_FILES)
	vvp $@

BUS_TB_FILES = $(TEST_MODULES)  $(VERILOG_MODULES) test/bus_interface_tb.v

bus_tb: $(BUS_TB_FILES)
	iverilog -o $@ $(BUS_TB_FILES)
	vvp $@

CRU_TB_FILES = $(TEST_MODULES)  $(VERILOG_MODULES) test/cru_tb.v

cru_tb: $(CRU_TB_FILES)
	iverilog -o $@ $(CRU_TB_FILES)
	vvp $@

