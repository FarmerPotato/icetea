/******************************************************************************
******************************************************************************/

module decoder_stub(input clk, input rst, output [2:0] dec);

	reg [7:0] count;

  assign dec[2] = count[7];
  assign dec[1] = 1'b0;  // the write modes all have 1
  assign dec[0] = count[6];
  
	always @(posedge clk)
	    if(rst)
                count = 0;
            else
		count <= count + 1;

endmodule
