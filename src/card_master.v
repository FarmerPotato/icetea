
module card_master(      input  clk,
                         input  en_cru,  
                         output o_driving_db, 
                         output reg o_data, 
                         input  [15:0] i_addr, 
                         input  [7:0] i_data, 
                         input  i_rd,
                         input  i_we,
                         input  i_cruclk,
                         input  i_cruout,
                         output o_cruin,
                         input  [0:32767] dsrrom
);

// $readmemh("master_dsr.mem", dsrrom);
  
  wire [7:0] bits;
  // only bits 0 and 7 are needed
  cru_register8 cru_reg(.clk(clk), 
                        .en(en_cru), 
                        .i_addr(i_addr[3:1]), 
                        .i_cruclk(i_cruclk), 
                        .i_cruout(i_cruout), 
                        .o_cruin(o_cruin), 
                        .o_bits(bits));
                        
  assign dsr = bits[0] && (3'b010 == i_addr[15:13]); // anywhere in >4000-5fff
  assign o_driving_db = i_rd && dsr;
  

  always @(posedge clk)
  begin
    if (i_rd && dsr)
      o_data <= dsrrom[i_addr[11:0]];
    else
      o_data <= 8'd0;
      
  end
  
endmodule