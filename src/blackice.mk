chip.bin: $(VERILOG_FILES) ${PCF_FILE} ${ROM_FILES}
	yosys -q -p "synth_ice40 -blif chip.blif" $(VERILOG_FILES)
	arachne-pnr -d 8k -P tq144:4k -p ${PCF_FILE} chip.blif -o chip.txt
	icepack chip.txt chip.bin

# the stty doesn't seem to prepare the device.
# try stty on the command line too if you get "rbits failed"

.PHONY: upload
upload: chip.bin
	stty -f ${DEVICE} raw
	(cat chip.bin; sleep 3) > ${DEVICE} 

.PHONY: clean
clean:
	$(RM) -f chip.blif chip.txt chip.bin

# Mac OS X:
# "make tty" in a separate terminal to keep the device pipe open.
# otherwise cat will close the pipe before all bytes are sent and
# upload will fail.
# https://forum.mystorm.uk/t/uploading-shenanigans/235/6
# The sleep 3 also takes care of that.

tty: 
	stty -f ${DEVICE} raw
	cat < ${DEVICE} 
