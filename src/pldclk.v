/******************************************************************************
******************************************************************************/

module pldclk(input clk, input rst, output clkout);

	reg [3:0] count;

  assign clkout = count[3];
 
	always @(posedge clk)
	    if(rst)
                count = 0;
            else
		count <= count + 1;

endmodule
