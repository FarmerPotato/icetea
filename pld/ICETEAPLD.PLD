Name     IceTeaPLD ;
PartNo   00 ;
Date     6/18/2019 ;
Revision 01 ;
Designer Engineer ;
Company  Erik Olson ;
Assembly None ;
Location  ;
Device   g22v10lcc ;

/* options:   cbld -e | grep g22v10      OE D AR SP */

/* for corrected pinout: */
/* *************** INPUT PINS *********************/
PIN  2  = CLKIN                   ; /*                               */ 
PIN  3  = MEMEN                   ; /*                            */ 
PIN  4  = RESET                   ; /*                            */ 
PIN  5  = DBIN                   ; /*                            */
PIN  6  = A15                   ; /*                                 */ 
PIN  7  = WE                      ; /* GND                             */ 
PIN  8  = GND                      ; /* GND                             */ 
PIN  9  = CRUCLK                   ; /*                                 */ 
/* PIN 10  NC */                   /*                                 */ 
/* PIN 11  NC  */                    /*                                 */ 
PIN 12  = DEC2                      ; /*                                 */ 
PIN 13  = DEC1                  ; /*                                 */ 
/* PIN 14  GND  */                   /*                                 */ 
/* PIN 15  GND  */                    /*                                 */ 
PIN 16  = DEC0                     ; /* GND                             */ 


/* *************** OUTPUT PINS *********************/
PIN 17  = ADEN1                   ; /*                                 */ 
PIN 18  = ADEN2                   ; /*                                 */ 
PIN 19  = CRUBIT                  ; /*                                 */ 
PIN 20  = CRUEN                   ; /*                                 */ 
PIN 21  = ENC0                    ; /*                                 */ 
/* PIN 22 GND */
PIN 23  = ENC1                    ; /*                                 */ 
PIN 24  = ENC2                    ; /*                                 */ 
PIN 25  = DBEN2                   ; /*                            */
PIN 26  = DBDIR                   ; /*                            */
PIN 27  = DBEN1                   ; /*                            */

/* outputs ENC are signals from PLD to FPGA */
/*	000 read CRU (default)      */
/*	001 read memory 1 LSB       */
/*	010 transition              */
/*	011 write memory 1 LSB      */
/*	100 write CRU               */
/* 	101 read memory 2 MSB       */
/*	110 write memory 2 MSB      */
/*	111 reset                   */


/* No negative logic declarations assumed; !WE really means WE* low) */
      
/* MSB */
ENC2 =  (RESET & MEMEN & !DBIN & WE & !CRUCLK) /* cru write  */
      # (RESET & !MEMEN & !DBIN & !WE & !A15)  /* write2     */
      # (RESET & !MEMEN & DBIN & WE & !A15)    /* read2      */
      # (!RESET);                              /* reset      */


ENC1 =  (RESET & !MEMEN & !DBIN & !WE )        /* write 1, write 2   */
      # (RESET & !MEMEN & !DBIN & WE)         /* transition */
      # (!RESET) ;                               /* reset      */

/* LSB */
ENC0 =  (RESET & !MEMEN & DBIN & WE )    /* read1, read 2    */
      # (RESET & !MEMEN & !DBIN & !WE & A15 )         /* write 1 */
      # (!RESET) ;                            /* reset      */
      
      
/* inputs DEC are signals from FPGA to PLD */
/* This decoder began as a '138 plus some AND gates */
/* 	000 read memory LSB         */
/*	001 read memory MSB (FUTURE)*/
/*	010 write memory LSB        */
/*	011 write memory MSB(FUTURE)*/
/*	100 read address MSB        */
/*	101 read address LSB        */
/*	110 drive CRUIN=0           */
/*	111 drive CRUIN=1           */

!DBEN1 = !DEC2 &         !DEC0 ;
!DBEN2 = !DEC2 &          DEC0 ;
!DBDIR = !DEC2 & !DEC1         ;
!ADEN1 =  DEC2 & !DEC1 & !DEC0 ;
!ADEN2 =  DEC2 & !DEC1 &  DEC0 ;
!CRUEN =  DEC2 &  DEC1         ;
CRUBIT =  DEC2 &  DEC1 &  DEC0 ;
