EESchema Schematic File Version 4
LIBS:ICET99-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 1800 850  0    60   ~ 0
Signals
Text Notes 5050 3000 0    60   ~ 0
Data Bus
Text Label 4450 3550 0    60   ~ 0
D3
Text Label 4450 3450 0    60   ~ 0
D2
Text Label 4450 3350 0    60   ~ 0
D1
Text Label 4450 3250 0    60   ~ 0
D0
Text Label 4450 3750 0    60   ~ 0
D5
Text Label 4450 3650 0    60   ~ 0
D4
Text Label 4450 3850 0    60   ~ 0
D6
Text Label 4450 3950 0    60   ~ 0
D7
Text Label 8400 4250 0    60   ~ 0
VCC
Text Label 8400 4350 0    60   ~ 0
GND
Text Label 7550 4350 0    60   ~ 0
GND
$Comp
L Device:C C5
U 1 1 5A6F342D
P 9150 1000
F 0 "C5" H 9175 1100 50  0000 L CNN
F 1 ".1uF" H 9175 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 9188 850 50  0001 C CNN
F 3 "" H 9150 1000 50  0001 C CNN
	1    9150 1000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5A6F346E
P 8650 1000
F 0 "C3" H 8675 1100 50  0000 L CNN
F 1 ".1uF" H 8675 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8688 850 50  0001 C CNN
F 3 "" H 8650 1000 50  0001 C CNN
	1    8650 1000
	1    0    0    -1  
$EndComp
Text Label 8050 850  0    60   ~ 0
VCC
Text Label 8050 1300 0    60   ~ 0
GND
$Comp
L ICET99-rescue:74LS245-ICET99-rescue U3
U 1 1 5A6D1947
P 5450 3750
F 0 "U3" H 5500 3550 50  0000 C CNN
F 1 "74LVC245A" H 5550 3350 50  0000 C CNN
F 2 "Gemini:DIP-20_W17.78m-LVC245A-Back" H 5450 3750 50  0001 C CNN
F 3 "" H 5450 3750 50  0001 C CNN
	1    5450 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 3450 4450 3450
Wire Wire Line
	4750 3950 4450 3950
Wire Wire Line
	8050 1150 8050 1300
Connection ~ 8350 1150
Connection ~ 8050 1150
Wire Wire Line
	8350 1150 8650 1150
Wire Wire Line
	8650 1150 8900 1150
Connection ~ 8350 850 
Wire Wire Line
	8350 850  8650 850 
Wire Wire Line
	8650 850  8900 850 
Wire Wire Line
	4750 3250 4450 3250
Wire Wire Line
	4750 3350 4450 3350
Wire Wire Line
	4750 3550 4450 3550
Wire Wire Line
	4750 3650 4450 3650
Wire Wire Line
	4750 3750 4450 3750
Wire Wire Line
	4750 3850 4450 3850
Text Label 1950 3450 2    60   ~ 0
CRUBIT
Text Label 1950 3350 2    60   ~ 0
CRUEN
Text Label 1950 3550 2    60   ~ 0
CRUIN
$Comp
L Device:C C4
U 1 1 5A73B531
P 8900 1000
F 0 "C4" H 8925 1100 50  0000 L CNN
F 1 ".1uF" H 8925 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8938 850 50  0001 C CNN
F 3 "" H 8900 1000 50  0001 C CNN
	1    8900 1000
	1    0    0    -1  
$EndComp
Connection ~ 8650 850 
Connection ~ 8650 1150
$Comp
L Device:R_Small R1
U 1 1 5A73B7FA
P 1150 3650
F 0 "R1" H 1180 3670 50  0000 L CNN
F 1 "1K" H 1180 3610 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1150 3650 50  0001 C CNN
F 3 "" H 1150 3650 50  0001 C CNN
	1    1150 3650
	0    -1   -1   0   
$EndComp
Text Notes 5050 3150 0    60   ~ 0
8-Bit
Text Label 2750 2450 0    60   ~ 0
GND
Text Label 4450 4150 0    60   ~ 0
DBDIR
Text Label 4450 4250 0    60   ~ 0
DBEN1
$Comp
L Device:C C1
U 1 1 5CF30AF0
P 8050 1000
F 0 "C1" H 8075 1100 50  0000 L CNN
F 1 ".1uF" H 8075 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 8088 850 50  0001 C CNN
F 3 "" H 8050 1000 50  0001 C CNN
	1    8050 1000
	1    0    0    -1  
$EndComp
Connection ~ 8900 850 
Connection ~ 8900 1150
Wire Wire Line
	4900 1300 5350 1300
Wire Wire Line
	5350 1400 4900 1400
Text Label 4900 1400 0    60   ~ 0
GND
Text Label 4900 1300 0    60   ~ 0
AUDIOIN
$Comp
L ICET99-rescue:CONN_01X02-conn T1
U 1 1 5A6E45B3
P 5550 1350
F 0 "T1" H 5550 1450 50  0000 C CNN
F 1 "Conn_01x02" H 5550 1150 50  0001 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02_Pitch2.54mm" H 5550 1350 50  0001 C CNN
F 3 "" H 5550 1350 50  0001 C CNN
	1    5550 1350
	1    0    0    -1  
$EndComp
Text Notes 4950 1200 0    60   ~ 0
Test Points
Text Label 2750 2850 2    60   ~ 0
VCC
Text Label 1950 1650 2    60   ~ 0
BPHI3*
Text Label 3550 1650 0    60   ~ 0
BWE*
Text Label 3550 3850 0    60   ~ 0
BCRUCLK
Text Label 1950 1850 2    60   ~ 0
MEMEN*
Text Label 3550 1550 0    60   ~ 0
WE*
Text Label 3550 3750 0    60   ~ 0
CRUCLK
Text Label 3550 1850 0    60   ~ 0
DBIN
Text Label 3550 3450 0    60   ~ 0
RESET*
Text Label 1950 1550 2    60   ~ 0
PHI3*
Text Notes 7400 7500 0    60   ~ 0
IceTea interface from TI-99/4A to PMod
Text Notes 8700 7650 2    60   ~ 0
10/26/2019
Text Label 5450 4300 3    60   ~ 0
GND
Text Label 8400 5150 0    60   ~ 0
VCC
Text Label 7550 4250 0    60   ~ 0
VCC
Text Label 7550 5250 0    60   ~ 0
GND
Text Label 7550 5150 0    60   ~ 0
VCC
Text Label 8400 5250 0    60   ~ 0
GND
Wire Wire Line
	7750 4550 7800 4550
Wire Wire Line
	7750 4650 7800 4650
Wire Wire Line
	7750 4750 7800 4750
NoConn ~ 7800 4850
NoConn ~ 8400 4850
NoConn ~ 8400 4950
NoConn ~ 8400 5050
NoConn ~ 7800 5050
NoConn ~ 7800 4950
$Comp
L power:+3.3V #PWR02
U 1 1 5CF2FC1C
P 8700 6100
F 0 "#PWR02" H 8700 5950 50  0001 C CNN
F 1 "+3.3V" H 8715 6273 50  0000 C CNN
F 2 "" H 8700 6100 50  0001 C CNN
F 3 "" H 8700 6100 50  0001 C CNN
	1    8700 6100
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR01
U 1 1 5CF3EBA3
P 7450 6100
F 0 "#PWR01" H 7450 5850 50  0001 C CNN
F 1 "GND" H 7455 5927 50  0000 C CNN
F 2 "" H 7450 6100 50  0001 C CNN
F 3 "" H 7450 6100 50  0001 C CNN
	1    7450 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 6100 7450 5850
Wire Wire Line
	8700 5150 8400 5150
Wire Wire Line
	8700 5150 8700 4250
Wire Wire Line
	8700 4250 8400 4250
Connection ~ 8700 5150
Wire Wire Line
	7450 5250 7450 4350
Connection ~ 7450 5250
Wire Wire Line
	7350 5150 7350 5750
Connection ~ 8700 5750
Wire Wire Line
	8700 5750 8700 5150
Wire Wire Line
	7350 5150 7350 4250
Connection ~ 7350 5150
Wire Wire Line
	8400 4350 8800 4350
Wire Wire Line
	8800 4350 8800 5250
Wire Wire Line
	8800 5850 7450 5850
Connection ~ 7450 5850
Wire Wire Line
	7450 5850 7450 5250
Wire Wire Line
	8400 5250 8800 5250
Connection ~ 8800 5250
Wire Wire Line
	8800 5250 8800 5850
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5CF9A201
P 8900 6100
F 0 "#FLG02" H 8900 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 8900 6273 50  0000 C CNN
F 2 "" H 8900 6100 50  0001 C CNN
F 3 "~" H 8900 6100 50  0001 C CNN
	1    8900 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 5750 8700 6100
Wire Wire Line
	7350 5750 8700 5750
Wire Wire Line
	8900 6100 8700 6100
Connection ~ 8700 6100
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5CFBCE57
P 7650 6100
F 0 "#FLG01" H 7650 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 7650 6273 50  0000 C CNN
F 2 "" H 7650 6100 50  0001 C CNN
F 3 "~" H 7650 6100 50  0001 C CNN
	1    7650 6100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7650 6100 7450 6100
Connection ~ 7450 6100
$Comp
L Mechanical:MountingHole H1
U 1 1 5CE594E8
P 10500 650
F 0 "H1" H 10600 696 50  0000 L CNN
F 1 "MountingHole" H 10600 605 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_3.2mm_M3" H 10500 650 50  0001 C CNN
F 3 "~" H 10500 650 50  0001 C CNN
	1    10500 650 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5CE59925
P 10500 900
F 0 "H2" H 10600 946 50  0000 L CNN
F 1 "MountingHole" H 10600 855 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.2mm_M2" H 10500 900 50  0001 C CNN
F 3 "~" H 10500 900 50  0001 C CNN
	1    10500 900 
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5CE59F12
P 10500 1150
F 0 "H3" H 10600 1196 50  0000 L CNN
F 1 "MountingHole" H 10600 1105 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.2mm_M2" H 10500 1150 50  0001 C CNN
F 3 "~" H 10500 1150 50  0001 C CNN
	1    10500 1150
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5CE59FEA
P 10500 1350
F 0 "H4" H 10600 1396 50  0000 L CNN
F 1 "MountingHole" H 10600 1305 50  0000 L CNN
F 2 "Mounting_Holes:MountingHole_2.2mm_M2" H 10500 1350 50  0001 C CNN
F 3 "~" H 10500 1350 50  0001 C CNN
	1    10500 1350
	1    0    0    -1  
$EndComp
Text Label 5450 4900 1    60   ~ 0
VCC
Text Notes 5050 6200 0    60   ~ 0
LSB
Text Notes 5050 4800 0    60   ~ 0
MSB
$Comp
L ICET99-rescue:74LVC245A-ICET99_Library U5
U 1 1 5CD0EE0E
P 5450 6900
F 0 "U5" H 5500 6700 50  0000 C CNN
F 1 "74LVC245A" H 5550 6500 50  0000 C CNN
F 2 "Gemini:DIP-20_W17.78m-LVC245A-Back" H 5450 6900 50  0001 C CNN
F 3 "" H 5450 6900 50  0001 C CNN
	1    5450 6900
	1    0    0    -1  
$EndComp
$Comp
L ICET99-rescue:74LVC245A-ICET99_Library U4
U 1 1 5CD0E9F9
P 5450 5450
F 0 "U4" H 5500 5250 50  0000 C CNN
F 1 "74LVC245A" H 5550 5050 50  0000 C CNN
F 2 "Gemini:DIP-20_W17.78m-LVC245A-Back" H 5450 5450 50  0001 C CNN
F 3 "" H 5450 5450 50  0001 C CNN
	1    5450 5450
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 6600 4350 6600
Wire Wire Line
	4750 6700 4350 6700
Wire Wire Line
	4750 7000 4350 7000
Wire Wire Line
	4750 6400 4350 6400
Wire Wire Line
	4750 6500 4350 6500
Wire Wire Line
	4750 6800 4350 6800
Wire Wire Line
	4750 6900 4350 6900
Text Notes 5050 4700 0    60   ~ 0
Address Bus\n
Text Label 4350 6400 0    60   ~ 0
A8
Text Label 4350 6500 0    60   ~ 0
A9
Text Label 4350 6600 0    60   ~ 0
A10
Text Label 4350 6700 0    60   ~ 0
A11
Text Label 4350 6800 0    60   ~ 0
A12
Text Label 4350 6900 0    60   ~ 0
A13
Text Label 4350 7000 0    60   ~ 0
A14
Text Label 4350 7100 0    60   ~ 0
A15CRUOUT
$Comp
L ICET99-rescue:CONN_02X22-conn J1
U 1 1 5A6D1DC3
P 1550 6000
F 0 "J1" H 1600 7100 50  0000 C CNN
F 1 "Conn_02x22_Odd_Even" H 1600 4800 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x22_Pitch2.54mm" H 1550 6000 50  0001 C CNN
F 3 "" H 1550 6000 50  0001 C CNN
	1    1550 6000
	-1   0    0    1   
$EndComp
Wire Wire Line
	1800 5750 2150 5750
Wire Wire Line
	1800 6250 2150 6250
NoConn ~ 1800 4950
NoConn ~ 1800 6450
NoConn ~ 1800 7050
NoConn ~ 1300 7050
NoConn ~ 1300 6950
NoConn ~ 1300 5750
Text Notes 900  4750 0    60   ~ 0
Console
Wire Wire Line
	1800 4950 2150 4950
Wire Wire Line
	1800 5050 2150 5050
Wire Wire Line
	1800 5150 2150 5150
Wire Wire Line
	1800 5250 2150 5250
Wire Wire Line
	1800 5350 2150 5350
Wire Wire Line
	1800 5450 2150 5450
Wire Wire Line
	1800 5550 2150 5550
Wire Wire Line
	1800 5650 2150 5650
Wire Wire Line
	1800 5850 2150 5850
Wire Wire Line
	1800 5950 2150 5950
Wire Wire Line
	1800 6050 2150 6050
Wire Wire Line
	1800 6150 2400 6150
Wire Wire Line
	1800 6350 2150 6350
Wire Wire Line
	1800 6450 2150 6450
Wire Wire Line
	1800 6550 2150 6550
Wire Wire Line
	1800 6650 2150 6650
Wire Wire Line
	1800 6750 2150 6750
Wire Wire Line
	1800 6850 2150 6850
Wire Wire Line
	1800 6950 2150 6950
Wire Wire Line
	1800 7050 2150 7050
Wire Wire Line
	1300 7050 1200 7050
Wire Wire Line
	1300 6950 1200 6950
Wire Wire Line
	1300 6850 1200 6850
Wire Wire Line
	1300 6750 1200 6750
Wire Wire Line
	1300 6650 1200 6650
Wire Wire Line
	1300 6550 1200 6550
Wire Wire Line
	1300 6450 1200 6450
Wire Wire Line
	1300 6350 1200 6350
Wire Wire Line
	1300 6250 1200 6250
Wire Wire Line
	1300 6150 1200 6150
Wire Wire Line
	1300 6050 1200 6050
Wire Wire Line
	1300 5950 1200 5950
Wire Wire Line
	1300 5850 1200 5850
Wire Wire Line
	1300 5750 1200 5750
Wire Wire Line
	1300 5650 1200 5650
Wire Wire Line
	1300 5550 1200 5550
Wire Wire Line
	1300 5450 1200 5450
Wire Wire Line
	1300 5350 1200 5350
Wire Wire Line
	1300 5250 1200 5250
Wire Wire Line
	1300 5150 1200 5150
Wire Wire Line
	1300 5050 1200 5050
Wire Wire Line
	1300 4950 1200 4950
Text Label 1200 4950 2    60   ~ 0
AUDIOIN
Text Label 1200 5050 2    60   ~ 0
D3
Text Label 1200 5150 2    60   ~ 0
D1
Text Label 1200 5250 2    60   ~ 0
D5
Text Label 1200 5350 2    60   ~ 0
D6
Text Label 1200 5450 2    60   ~ 0
D7
Text Label 1200 5550 2    60   ~ 0
MEMEN*
Text Label 1200 5650 2    60   ~ 0
A1
Text Label 1200 5750 2    60   ~ 0
MBE*
Text Label 1200 5850 2    60   ~ 0
WE*
Text Label 1200 5950 2    60   ~ 0
PHI3*
Text Label 1200 6050 2    60   ~ 0
CRUCLK
Text Label 1200 6150 2    60   ~ 0
A2
Text Label 1200 6250 2    60   ~ 0
A9
Text Label 1200 6350 2    60   ~ 0
A14
Text Label 1200 6450 2    60   ~ 0
A8
Text Label 1200 6550 2    60   ~ 0
READY
Text Label 1200 6650 2    60   ~ 0
A3
Text Label 1200 6750 2    60   ~ 0
A11
Text Label 1200 6850 2    60   ~ 0
A10
Text Label 1200 6950 2    60   ~ 0
EXTINT*
Text Label 2150 4950 2    60   ~ 0
MINUS5V
Text Label 2150 5050 2    60   ~ 0
IAQ
Text Label 2150 5150 2    60   ~ 0
D2
Text Label 2150 5250 2    60   ~ 0
D0
Text Label 2150 5350 2    60   ~ 0
D4
Text Label 2150 5450 2    60   ~ 0
CRUIN
Text Label 2150 5550 2    60   ~ 0
A0
Text Label 2150 5650 2    60   ~ 0
A6
Text Label 2150 5750 2    60   ~ 0
GND
Text Label 2150 5850 2    60   ~ 0
GND
Text Label 2150 5950 2    60   ~ 0
GND
Text Label 2150 6050 2    60   ~ 0
GND
Text Label 2400 6150 2    60   ~ 0
A15CRUOUT
Text Label 2150 6250 2    60   ~ 0
A7
Text Label 2150 6350 2    60   ~ 0
A13
Text Label 2150 6450 2    60   ~ 0
LOAD*
Text Label 2150 6550 2    60   ~ 0
A12
Text Label 2150 6650 2    60   ~ 0
DBIN
Text Label 2150 6750 2    60   ~ 0
A4
Text Label 2150 6850 2    60   ~ 0
A5
Text Label 2150 6950 2    60   ~ 0
RESET*
Text Label 2150 7050 2    60   ~ 0
PLUS5V
Text Label 1200 7050 2    60   ~ 0
SBE
Wire Wire Line
	4350 7100 4750 7100
Text Notes 7300 5500 1    60   ~ 0
pins nearest edge
Text Notes 8900 5400 1    60   ~ 0
pins on interior
Wire Wire Line
	7450 4350 7800 4350
Wire Wire Line
	7450 5250 7800 5250
Wire Wire Line
	7350 5150 7800 5150
Wire Wire Line
	7350 4250 7800 4250
$Comp
L ICET99-rescue:MxMod-Device-ICET99_Library J2
U 1 1 5CE27641
P 8050 4950
F 0 "J2" H 8100 5875 50  0000 C CNN
F 1 "MxMod-Device" H 8100 5784 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_2x15_Pitch2.54mm" V 8050 4950 50  0001 C CNN
F 3 "~" H 8050 4950 50  0001 C CNN
	1    8050 4950
	1    0    0    -1  
$EndComp
Text Label 3550 3550 0    60   ~ 0
BRESET*
Text Label 3550 1950 0    60   ~ 0
BDBIN
Wire Wire Line
	7750 4450 7800 4450
Text Label 1950 1950 2    60   ~ 0
BMEMEN*
$Comp
L Device:R_Small R2
U 1 1 5A6D7B33
P 4200 4150
F 0 "R2" H 4230 4170 50  0000 L CNN
F 1 "1K" H 4230 4110 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 4200 4150 50  0001 C CNN
F 3 "" H 4200 4150 50  0001 C CNN
	1    4200 4150
	0    1    1    0   
$EndComp
Text Label 3900 4550 2    60   ~ 0
VCC
Text Label 4000 7600 2    60   ~ 0
VCC
Wire Wire Line
	4000 7600 4100 7600
Wire Wire Line
	4100 7300 4750 7300
Wire Wire Line
	4100 7300 4100 5850
Connection ~ 4100 7300
Wire Wire Line
	4100 5850 4750 5850
Text Label 2750 950  2    60   ~ 0
VCC
Wire Wire Line
	4100 7300 4100 7600
Wire Wire Line
	8050 1150 8350 1150
Wire Wire Line
	8050 850  8350 850 
Wire Wire Line
	8900 850  9150 850 
Wire Wire Line
	8900 1150 9150 1150
Wire Wire Line
	8350 950  8350 850 
$Comp
L Device:C C2
U 1 1 5A6F3336
P 8350 1000
F 0 "C2" H 8375 1100 50  0000 L CNN
F 1 ".1uF" H 8375 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_0805_HandSoldering" H 8388 850 50  0001 C CNN
F 3 "" H 8350 1000 50  0001 C CNN
	1    8350 1000
	1    0    0    -1  
$EndComp
Text Label 10650 7650 0    60   ~ 0
4
Text Label 6150 6700 0    60   ~ 0
BA11
Text Label 6150 6500 0    60   ~ 0
BA9
Text Label 6150 7000 0    60   ~ 0
BA14
Text Label 6150 7100 0    60   ~ 0
BA15
Text Label 6150 6600 0    60   ~ 0
BA10
Text Label 6150 6400 0    60   ~ 0
BA8
Text Label 6150 6800 0    60   ~ 0
BA12
Text Label 6150 6900 0    60   ~ 0
BA13
Text Label 6150 3550 0    60   ~ 0
BD3
Text Label 6150 3350 0    60   ~ 0
BD1
Text Label 6150 3850 0    60   ~ 0
BD6
Text Label 6150 3950 0    60   ~ 0
BD7
Text Label 6150 3450 0    60   ~ 0
BD2
Text Label 6150 3250 0    60   ~ 0
BD0
Text Label 6150 3650 0    60   ~ 0
BD4
Text Label 6150 3750 0    60   ~ 0
BD5
Text Label 6350 5350 2    60   ~ 0
BA4
Text Label 6350 4950 2    60   ~ 0
BA0
Text Label 6350 5150 2    60   ~ 0
BA2
Text Label 6350 5050 2    60   ~ 0
BA1
Text Label 6350 5250 2    60   ~ 0
BA3
Text Label 6350 5450 2    60   ~ 0
BA5
Text Label 6350 5550 2    60   ~ 0
BA6
Text Label 6350 5650 2    60   ~ 0
BA7
Wire Wire Line
	4350 5250 4750 5250
Wire Wire Line
	4350 5150 4750 5150
Wire Wire Line
	4350 5050 4750 5050
Wire Wire Line
	4350 4950 4750 4950
Wire Wire Line
	4350 5650 4750 5650
Wire Wire Line
	4350 5550 4750 5550
Wire Wire Line
	4350 5450 4750 5450
Wire Wire Line
	4350 5350 4750 5350
Text Label 4350 5650 0    60   ~ 0
A7
Text Label 4350 5550 0    60   ~ 0
A6
Text Label 4350 5450 0    60   ~ 0
A5
Text Label 4350 5350 0    60   ~ 0
A4
Text Label 4350 5250 0    60   ~ 0
A3
Text Label 4350 5150 0    60   ~ 0
A2
Text Label 4350 5050 0    60   ~ 0
A1
Text Label 4350 4950 0    60   ~ 0
A0
Wire Wire Line
	6150 5650 6350 5650
Wire Wire Line
	6350 5550 6150 5550
Wire Wire Line
	6150 5450 6350 5450
Wire Wire Line
	6350 5350 6150 5350
Wire Wire Line
	6150 5250 6350 5250
Wire Wire Line
	6350 5150 6150 5150
Wire Wire Line
	6150 5050 6350 5050
Wire Wire Line
	6350 4950 6150 4950
Wire Wire Line
	4000 7750 4200 7750
Wire Wire Line
	4200 7750 4200 7400
Wire Wire Line
	4200 5950 4750 5950
Wire Wire Line
	4200 7400 4750 7400
Connection ~ 4200 7400
Wire Wire Line
	4200 7400 4200 5950
Text Label 4000 7750 2    60   ~ 0
GND
Wire Wire Line
	4100 5850 4100 4150
Connection ~ 4100 5850
Wire Wire Line
	4300 4150 4750 4150
Wire Wire Line
	4200 4250 4200 5950
Wire Wire Line
	4200 4250 4750 4250
Connection ~ 4200 5950
Text Label 4350 5950 0    60   ~ 0
ABEN1
Text Label 4350 7400 0    60   ~ 0
ABEN2
$Comp
L Gemini:74LVC125A U1
U 1 1 5DC6B5F6
P 2750 1700
F 0 "U1" H 2750 2700 50  0000 C CNN
F 1 "74LVC125A" H 2750 2600 50  0000 C CNN
F 2 "Gemini:DIP-14_W17.78m-LVC125A-Back" H 2750 1700 50  0001 C CNN
F 3 "" H 2750 1700 50  0001 C CNN
	1    2750 1700
	1    0    0    -1  
$EndComp
$Comp
L Gemini:74LVC125A U2
U 1 1 5DC819CC
P 2750 3600
F 0 "U2" H 2750 4600 50  0000 C CNN
F 1 "74LVC125A" H 2750 4500 50  0000 C CNN
F 2 "Gemini:DIP-14_W17.78m-LVC125A-Back" H 2750 3600 50  0001 C CNN
F 3 "" H 2750 3600 50  0001 C CNN
	1    2750 3600
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R3
U 1 1 5DC8BF37
P 1150 3350
F 0 "R3" V 1000 3350 50  0000 L CNN
F 1 "1K" V 1050 3350 50  0000 L CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" H 1150 3350 50  0001 C CNN
F 3 "" H 1150 3350 50  0001 C CNN
	1    1150 3350
	0    1    1    0   
$EndComp
Text Label 1050 3100 0    60   ~ 0
VCC
$Comp
L Device:C C6
U 1 1 5DCB270D
P 9450 1000
F 0 "C6" H 9475 1100 50  0000 L CNN
F 1 ".1uF" H 9475 900 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 9488 850 50  0001 C CNN
F 3 "" H 9450 1000 50  0001 C CNN
	1    9450 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	9150 850  9450 850 
Connection ~ 9150 850 
Wire Wire Line
	9450 1150 9150 1150
Connection ~ 9150 1150
Wire Wire Line
	1950 3350 1250 3350
Wire Wire Line
	1050 3100 1050 3350
Wire Wire Line
	1950 3650 1250 3650
Wire Wire Line
	1050 3650 1050 3350
Connection ~ 1050 3350
Wire Wire Line
	1950 1450 1400 1450
Wire Wire Line
	1400 1450 1400 1750
Wire Wire Line
	1950 1750 1400 1750
Connection ~ 1400 1750
Wire Wire Line
	1400 1750 1400 2250
Wire Wire Line
	3550 1450 4150 1450
Wire Wire Line
	4150 1450 4150 1750
Wire Wire Line
	3550 1750 4150 1750
Connection ~ 4150 1750
Wire Wire Line
	4150 1750 4150 2250
Wire Wire Line
	1400 2250 2750 2250
Wire Wire Line
	2750 2450 2750 2250
Connection ~ 2750 2250
Wire Wire Line
	2750 2250 4150 2250
Wire Wire Line
	1950 3750 1400 3750
Wire Wire Line
	1400 3750 1400 4150
Wire Wire Line
	1400 4150 2750 4150
Wire Wire Line
	2750 4150 2750 4350
Wire Wire Line
	2750 4150 4000 4150
Wire Wire Line
	4000 4150 4000 3650
Wire Wire Line
	4000 3650 3550 3650
Connection ~ 2750 4150
Wire Wire Line
	3550 3350 4000 3350
Wire Wire Line
	4000 3350 4000 3650
Connection ~ 4000 3650
$Comp
L Gemini:DIN-5 J6
U 1 1 5DB77D93
P 9450 1650
F 0 "J6" H 9450 1375 50  0000 C CNN
F 1 "DIN-5" H 9450 1284 50  0000 C CNN
F 2 "Connect:SDS-50J" H 9450 1650 50  0001 C CNN
F 3 "http://www.mouser.com/ds/2/18/40_c091_abd_e-75918.pdf" H 9450 1650 50  0001 C CNN
	1    9450 1650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J3
U 1 1 5DB78F80
P 8000 3500
F 0 "J3" H 8050 3917 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 8050 3826 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x06_Pitch2.54mm" H 8000 3500 50  0001 C CNN
F 3 "~" H 8000 3500 50  0001 C CNN
	1    8000 3500
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J4
U 1 1 5DB87AAC
P 8000 2550
F 0 "J4" H 8050 2967 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 8050 2876 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x06_Pitch2.54mm" H 8000 2550 50  0001 C CNN
F 3 "~" H 8000 2550 50  0001 C CNN
	1    8000 2550
	1    0    0    -1  
$EndComp
Text Label 8300 3300 0    60   ~ 0
VCC
Text Label 8300 2350 0    60   ~ 0
VCC
Text Label 8300 2450 0    60   ~ 0
GND
Text Label 8300 3400 0    60   ~ 0
GND
Text Label 7800 3500 2    60   ~ 0
BD0
Text Label 8300 3500 0    60   ~ 0
BD1
Text Label 8300 3600 0    60   ~ 0
BD3
Text Label 8300 3700 0    60   ~ 0
BD5
Text Label 8300 3800 0    60   ~ 0
BD7
Text Label 7800 3600 2    60   ~ 0
BD2
Text Label 7800 3700 2    60   ~ 0
BD4
Text Label 7800 3800 2    60   ~ 0
BD6
Text Label 7800 3300 2    60   ~ 0
VCC
Text Label 7800 3400 2    60   ~ 0
GND
Text Label 7800 2450 2    60   ~ 0
GND
Text Label 7800 2350 2    60   ~ 0
VCC
Text Label 7800 2550 2    60   ~ 0
BPHI3
Text Label 7800 2650 2    60   ~ 0
BMEMEN
Text Label 7800 2750 2    60   ~ 0
BDBIN
Text Label 7800 2850 2    60   ~ 0
BWE
Text Label 8300 2550 0    60   ~ 0
BCRUCLK
Text Label 8300 2650 0    60   ~ 0
CRUBIT
Text Label 8300 2750 0    60   ~ 0
CRUEN
Text Label 7750 4450 2    60   ~ 0
BA0
Text Label 7750 4550 2    60   ~ 0
BA2
Text Label 7750 4650 2    60   ~ 0
BA4
Text Label 7750 4750 2    60   ~ 0
BA6
Text Label 7750 5350 2    60   ~ 0
BA8
Text Label 7750 5450 2    60   ~ 0
BA10
Text Label 7750 5550 2    60   ~ 0
BA12
Text Label 7750 5650 2    60   ~ 0
BA14
Wire Wire Line
	7750 5650 7800 5650
Wire Wire Line
	7800 5550 7750 5550
Wire Wire Line
	7750 5450 7800 5450
Wire Wire Line
	7800 5350 7750 5350
Text Label 8400 5650 0    60   ~ 0
BA15
Text Label 8400 5550 0    60   ~ 0
BA13
Text Label 8400 5450 0    60   ~ 0
BA11
Text Label 8400 5350 0    60   ~ 0
BA9
Text Label 8400 4750 0    60   ~ 0
BA7
Text Label 8400 4650 0    60   ~ 0
BA5
Text Label 8400 4550 0    60   ~ 0
BA3
Text Label 8400 4450 0    60   ~ 0
BA1
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J5
U 1 1 5DBBCA32
P 9400 2550
F 0 "J5" H 9450 2967 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 9450 2876 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x06_Pitch2.54mm" H 9400 2550 50  0001 C CNN
F 3 "~" H 9400 2550 50  0001 C CNN
	1    9400 2550
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x06_Odd_Even J6
U 1 1 5DBC19CF
P 10600 2550
F 0 "J6" H 10650 2967 50  0000 C CNN
F 1 "Conn_02x06_Odd_Even" H 10650 2876 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Angled_2x06_Pitch2.54mm" H 10600 2550 50  0001 C CNN
F 3 "~" H 10600 2550 50  0001 C CNN
	1    10600 2550
	1    0    0    -1  
$EndComp
Text Label 9700 2350 0    60   ~ 0
VCC
Text Label 10900 2350 0    60   ~ 0
VCC
Text Label 9200 2350 2    60   ~ 0
VCC
Text Label 10400 2350 2    60   ~ 0
VCC
Text Label 9700 2450 0    60   ~ 0
GND
Text Label 10900 2450 0    60   ~ 0
GND
Text Label 9200 2450 2    60   ~ 0
GND
Text Label 10400 2450 2    60   ~ 0
GND
Text Label 10900 2550 0    60   ~ 0
SDIN
Text Label 10900 2650 0    60   ~ 0
SCK
Text Label 10900 2750 0    60   ~ 0
LRCK
Text Label 10900 2850 0    60   ~ 0
MCLK
Text Label 10400 2550 2    60   ~ 0
MIDIIN
Text Label 10400 2650 2    60   ~ 0
MIDIOUT
Text Label 8300 2850 0    60   ~ 0
DBDIR
$EndSCHEMATC
